#%%
from training import Training

t = Training()
t.df.head()

# %%
sorted(list(t.df['phase'].unique()))

# %%
t.df[t.df['phase'] == 3]['week'].unique()

# %%
len(t.weeks(3))
# %%
t.df[t.df['week'] == 9].index.tolist()

# %%
dict(t.df.loc[14])

# %%
t.df[t.df['week'] == 9]

# %%
t.df[t.df['wday'] == 3]

# %%
x = t.df[(t.df['week'] == 9) & (t.df['wday'] == 3)]
dict(x.iloc[0])

# %%
import re

for w in t.df['workout'].to_list():
    w_split = re.split('(\+)', w)
    if len(w_split) < 3:
        continue
    if ('E' in w_split[0]) and ('E' in w_split[-1]):
        wu = w_split[0].strip()
        cd = w_split[-1].strip()

        wr = "".join(w_split[2:-2]).strip()

        print(w)
        print(f"WU: {wu} / CD: {cd}")
        print(wr)
        print()


# %%
import re

x = t.df['workout'].to_list()[11]
re.split('(\+)', x)

# %%
t.df.head()




# %%


# %%

t.df.head()

# %%
import pandas as pd
m_list = [(x.year,x.month) for x in t.df['date']]
m_list = pd.Series(m_list)
m_list = m_list.unique()
m_list = list(m_list)
m_list
# %%
t.df['date'][45].strftime("%B")

# %%
import calendar
import numpy as np

_, ndays = calendar.monthrange(2017, 12)
l = list(range(1, ndays+1))
print(l)

for day in l:
    dayarray = np.squeeze(np.asarray(day))
    print(dayarray)
# %%
for day in list(calendar.monthcalendar(2017,4)):
    print(day)
# %%
calendar.monthcalendar(2017,4)
#%%
from training import Training
from pprint import pprint as pp


t = Training()
c = t.calendar()
pp(c)

# %%
from training import Training
from pprint import pprint as pp


t = Training()
t.df.head()

# %%
t.df[t.df['date'] == '2021-05-00']['phase']

# %%
t.phase(2021,5,4)
# %%
t.phase(2021,5,3)
# %%
