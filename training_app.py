from flask import *
from training import Training

app = Flask(__name__)


@app.route("/calendar")
def show_calendar():

    t = Training()

    return render_template('calendar.html', t=t,)


@app.route("/table")
def show_table():

    t = Training()
    header = [
        'phase', 'week',
        '1', '2', '3', '4', '5', '6', '7',
        'total',
    ]

    return render_template('table.html', t=t, header=header)


@app.route("/view")
def show_view():

    t = Training()
    header = [
        'phase', 'week',
        '1', '2', '3', '4', '5', '6', '7',
        'total',
    ]

    return render_template('view.html', t=t, header=header,)


if __name__ == "__main__":
    app.run(debug=True)