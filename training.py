import configparser
import calendar
import datetime as dt
import pandas as pd
import re


class Training():
        
    DEFAULT_CONFIG = 'training.conf'
    DEFAULT_SECTION = 'DEFAULT'
    NAMES = {
        'E':'Easy',
        'R':'Repetitions',
        'I':'Interval',
        'L':'Long',
        'E opt':'Easy',
        'T':'Threshold',
        'M':'Marathon'
    }
    PHASE = {
        1:'Base',
        2:'Initial',
        3:'Transition',
        4:'Final',
    }


    def __init__(
        self, 
        conf_file: str = DEFAULT_CONFIG,
        conf_section: str = DEFAULT_SECTION,
    ):

        self.conf_file = conf_file
        self.conf_section = conf_section

        self.plan_path, self.start_date = self._load_config()
        self.plan = self._load_plan()

        self.df = self._make_training()
        self.df = self._make_wucd()
        self.df = self._make_names()


    def __str__(self) -> str:

        return self.training.to_string()


    def _load_config(
        self,
        conf_file: str = None,
        conf_section: str = None,
    ) -> (str, dt.datetime):

        if not conf_file: conf_file = self.conf_file
        if not conf_section: conf_section = self.conf_section

        config = configparser.ConfigParser()
        config.read(conf_file)

        plan_path = [
            config.get(conf_section, 'plan_folder'),
            '/',
            config.get(conf_section, 'plan_file'),
            '.csv',
        ]
        plan_path = "".join(plan_path)

        start_date = dt.datetime.strptime(
            config.get(conf_section, 'start_date'), 
            '%Y-%m-%d'
        )

        return plan_path, start_date


    def _load_plan(
        self,
        plan_path: str = None,
    ) -> pd.DataFrame:

        if not plan_path: plan_path = self.plan_path

        plan = pd.read_csv(
            plan_path, sep=',', skip_blank_lines=True,
            skipinitialspace=True,
        )
        plan = plan.dropna(axis='columns')

        plan['w_start'] = [int(x[0]) for x in plan['week'].str.split('-')]
        plan['w_end'] = [int(x[1]) for x in plan['week'].str.split('-')]

        return plan


    def _make_training(
        self,
        plan: pd.DataFrame = None, 
        start_date: dt.datetime = None,
    ) -> pd.DataFrame:

        if not plan: plan = self.plan
        if not start_date: start_date = self.start_date

        t = pd.DataFrame()

        for w_num in range(plan['w_start'].min(), plan['w_end'].max() + 1):
            temp = plan[
                (plan['w_start'] <= w_num) & (plan['w_end'] >= w_num)
            ].copy()
            temp['week'] = w_num
            t = t.append(temp, ignore_index=True)

        t = t.drop(['w_start', 'w_end'], axis='columns')

        t['date'] = 7 * (t['week'] - 1) + t['wday']
        t['date'] = [dt.timedelta(days=(day - 1)) for day in t['date']]
        t['date'] = start_date + t['date']

        t = t[['phase', 'week', 'wday', 'date', 'type', 'workout']]

        return t


    def _make_wucd(
        self,
        df: pd.DataFrame = None,
    ) -> pd.DataFrame:

        if not df: df = self.df

        df.insert(6, "wu", "")
        df.insert(7, "cd", "")

        for index, row in df.iterrows():
            w_split = re.split('(\+)', row['workout'])
            
            if len(w_split) < 5:
                df.at[index,'workout'] = row['workout'].replace(' ', '')
                continue

            if ('E' in w_split[0]) and ('E' in w_split[-1]):
                df.at[index,'wu'] = w_split[0].strip()
                df.at[index,'cd'] = w_split[-1].strip()
                workout = "".join(w_split[2:-2]).strip()
                df.at[index,'workout'] = workout.replace(' ', '')

        return df


    def _make_names(
        self,
        df: pd.DataFrame = None,
        names: dict = NAMES,
    ) -> pd.DataFrame:

        if not df: df = self.df

        df['optional'] = [('opt' in x) for x in df['type']]       
        df['type'] = df['type'].map(names)

        return df


    def phases(self) -> list:
        
        phases = self.df['phase'].unique()
        phases = sorted(phases.tolist())

        return phases


    def weeks(self, phase: int) -> list:

        weeks = self.df[self.df['phase'] == phase]['week'].unique()
        weeks = sorted(weeks.tolist())

        return weeks


    def days(self, week: int) -> list:

        days = self.df[self.df['week'] == week]
        days = days.index.tolist()
        return days


    def day(self, week: int, wday: int) -> list:
        
        day = self.df[(self.df['week'] == week) & (self.df['wday'] == wday)]
        day = dict(day.iloc[0])
        return day


    def name(self, phase: int = None) -> str:

        return Training.PHASE[phase]


    def calendar(self) -> list:

        months = [(x.year, x.month, x.strftime("%B")) for x in self.df['date']]
        months = pd.Series(months)
        months = months.unique()
        months = list(months)

        # cal = dict(months=months)
        cal = dict()

        for month in months:
            mweeks = calendar.monthcalendar(month[0],month[1])

            if len(mweeks) < 6:
                mweeks = mweeks + [[0]*7] * (6- len(mweeks))

            cal[month] = mweeks


        return cal

    def phase(self, year:int, month:int, day:int) -> str:

        date = f'{year}-{month:2}-{day:2}'
        try:
            phase = self.df[self.df['date'] == date]['phase'].values[0]
        except IndexError:
            return None
        else:
            return self.PHASE[phase]


def main():

    t = Training()
    print(t)


if __name__ == "__main__":

    main()
